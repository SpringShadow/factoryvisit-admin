import Layout from '@/layout'

const systemRouter = {
    path: '/system',
    component: Layout,
    meta: {
        title: '系统管理',
        icon: 'system'
    },
    name: '系统管理',
    redirect: '/system/userList',
    children: [
        {
            path: '/system/userList',
            component: () => import('@/views/system/userList/index'),
            name: '用户列表',
            meta: {
                title: '用户列表',
                icon: '',
                roles: ['admin']
            },
            children:[
                {
                    path: '/system/newUser',
                    component: () => import('@/views/system/userList/newUser'),
                    roles: ['admin'],
                    name: '编辑账号',
                    meta: {
                        title: '新建',
                        icon: '',
                        roles: ['admin']
                    },
                },
            ]
        },
        {
            path: '/system/rolesList',
            component: () => import('@/views/system/rolesList/index'),
            name: '角色列表',
            meta: {
                title: '角色列表',
                icon: '',
                roles: ['admin']
            },
            children:[
                {
                    path: '/system/newRole',
                    component: () => import('@/views/system/rolesList/newRole'),
                    roles: ['admin'],
                    name: '新建',
                    meta: {
                        title: '新建',
                        icon: '',
                        roles: ['admin']
                    },
                },
                {
                    path: '/system/viewRole',
                    component: () => import('@/views/system/rolesList/viewRole'),
                    roles: ['admin'],
                    name: '查看',
                    meta: {
                        title: '查看',
                        icon: '',
                        roles: ['admin']
                    },
                },
            ]
        },
        {
            path: '/system/resource',
            component: () => import('@/views/system/resource/index'),
            name: '资源管理',
            meta: {
                title: '资源管理',
                icon: '',
                roles: ['admin']
            },
        },
        {
            path: '/system/equipment',
            component: () => import('@/views/system/equipment/index'),
            name: '设备管理',
            meta: {
                title: '设备管理',
                icon: '',
                roles: ['admin']
            },
            children:[
                {
                    path: '/system/edit',
                    component: () => import('@/views/system/equipment/edit'),
                    roles: ['admin'],
                    name: '编辑',
                    meta: {
                        title: '编辑',
                        icon: '',
                        roles: ['admin']
                    },
                },
            ]
        },
        {
            path: '/system/template',
            component: () => import('@/views/system/template/index'),
            name: '模板管理',
            meta: {
                title: '模板管理',
                icon: '',
                roles: ['admin']
            },
        },
        {
            path: '/system/appSetting',
            component: () => import('@/views/system/appSetting/index'),
            name: 'APP设置',
            meta: {
                title: 'APP设置',
                icon: '',
                roles: ['admin']
            },
        },

    ]

}

export default systemRouter
