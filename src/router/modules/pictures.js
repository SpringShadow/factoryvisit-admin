import Layout from '@/layout'

const picturesRouter = {
    path: '/pictures',
    component: Layout,
    meta: {
        title: '图库',
        icon: 'pictures'
    },
    name: '图库',
    redirect: '/pictures/summary',
    children: [{
        path: '/pictures/summary',
        component: () => import('@/views/pictures/summary'),
        name: '图片汇总',
        meta: {
            title: '图片汇总',
            icon: '',
            roles: ['admin']
        },
    }],
    alwaysShow: true,
}

export default picturesRouter
