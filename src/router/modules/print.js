import Layout from '@/layout'

const printRouter = {
    path: '/print',
    component: Layout,
    meta: {
        title: '打印管理',
        icon: 'print'
    },
    name: '打印管理',
    redirect: '/print/record',
    children: [{
            path: '/print/record',
            component: () => import('@/views/print/record/index'),
            name: '',
            meta: {
                title: '打印记录',
                icon: '',
                roles: ['admin']
            },
        },
        {
            path: '/print/card',
            component: () => import('@/views/print/record/index'),
            name: '',
            meta: {
                title: '点卡管理',
                icon: '',
                roles: ['admin']
            },
        }
    ],

}

export default printRouter
