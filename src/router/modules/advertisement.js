import Layout from '@/layout'

const advertiseRouter = {
    path: '/advertisement',
    component: Layout,
    meta: {
        title: '广告管理',
        icon: 'advertise'
    },
    name: '广告管理',
    redirect: '/advertisement/management',
    children: [
        {
            path: '/advertisement/management',
            component: () => import('@/views/advertisement/management/index'),
            name: '广告',
            meta: {
                title: '广告',
                icon: '',
                roles: ['admin'],
                noTransition:true
            },
            children:[
                {
                    path: '/advertisement/new',
                    component: () => import('@/views/advertisement/management/new'),
                    roles: ['admin'],
                    name: '新增',
                    meta: {
                        title: '新增',
                        icon: '',
                        roles: ['admin'],
                        noTransition:true
                    },
                },
            ]
        },
        {
            path: '/advertisement/position',
            component: () => import('@/views/advertisement/position/index'),
            name: '广告位',
            meta: {
                title: '广告位',
                icon: '',
                roles: ['admin'],
                noTransition:true
            },
            children:[
                {
                    path: '/advertisement/edit',
                    component: () => import('@/views/advertisement/position/edit'),
                    roles: ['admin'],
                    name: '',
                    meta: {
                        title: '编辑',
                        icon: '',
                        roles: ['admin'],
                        noTransition:true
                    },
                },
            ]
        },
    ]
}

export default advertiseRouter
