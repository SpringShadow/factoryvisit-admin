import Layout from '@/layout'

const customerRouter = {
    path: '/customerManage',
    component: Layout,
    meta: {
        title: '客户管理',
        icon: 'customer'
    },
    name: '客户管理',
    redirect: '/customerManage/summary',
    children: [{
        path: '/customerManage/summary',
        component: () => import('@/views/customerManage/index'),
        name: '客户汇总',
        meta: {
            title: '客户汇总',
            icon: '',
            roles: ['admin']
        },
        children: [{
            path: '/customerManage/detail',
            component: () => import('@/views/customerManage/detail'),
            name: '客户详情',
            meta: {
                title: '客户详情',
                icon: '',
                roles: ['admin']
            },
            hidden: true,
            children: [{
                path: '/customerManage/pictures',
                component: () => import('@/views/customerManage/pictures'),
                name: 'TA的照片',
                meta: {
                    title: 'TA的照片',
                    icon: '',
                    roles: ['admin']
                },
                hidden: true
            }, ]
        }, ]
    }]
}

export default customerRouter
