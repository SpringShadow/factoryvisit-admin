import Layout from '@/layout'

const marketRouter = {
        path: '/marketActivity',
        component: Layout,
        meta: {
            title: '市场活动',
            icon: 'market',
            roles: ['admin']
        },
        name: '市场活动',
        redirect: '/marketActivity/list',
        children: [{
            path: '/marketActivity/list',
            component: () => import('@/views/marketActivity/index'),
            name: '活动列表',
            meta: {
                title: '活动列表',
                icon: '',
                roles: ['admin']
            },
            children:[
               {
                   path: '/marketActivity/new',
                   component: () => import('@/views/marketActivity/newMarketActivity'),
                   name: '编辑活动',
                   meta: {
                       title: '编辑活动',
                       icon: '',
                       roles: ['admin']
                   },
                   hidden: true,
               },
               {
                   path: '/marketActivity/detail',
                   component: () => import('@/views/marketActivity/marketActivityDetail'),
                   name: '活动详情',
                   meta: {
                       title: '活动详情',
                       icon: '',
                       roles: ['admin']
                   },
                   hidden: true,
               },
            ]
        }],
        alwaysShow: true
}

export default marketRouter
