import Layout from '@/layout'

const salesRouter = {
    path: '/salesActivity',
    component: Layout,
    meta: {
        title: '营销活动',
        icon: 'sales'
    },
    name: '营销活动',
    redirect: '/salesActivity/summary',
    children: [{
            path: '/salesActivity/summary',
            component: () => import('@/views/salesActivity/index'),
            name: '活动汇总',
            meta: {
                title: '活动汇总',
                icon: '',
                roles: ['admin']
            },
            children:[
               {
                   path: '/salesActivity/new',
                   component: () => import('@/views/salesActivity/newSalesActivity'),
                   name: '',
                   meta: {
                       title: '编辑活动',
                       icon: '',
                       roles: ['admin']
                   },
                   hidden: true,
               },
               {
                   path: '/salesActivity/detail',
                   component: () => import('@/views/salesActivity/salesActivityDetail'),
                   name: '',
                   meta: {
                       title: '活动详情',
                       icon: '',
                       roles: ['admin']
                   },
                   hidden: true,
               },
            ]
        },
        {
            path: '/salesActivity/record',
            component: () => import('@/views/salesActivity/record'),
            name: '核销记录',
            meta: {
                title: '核销记录',
                icon: '',
                roles: ['admin']
            },
        },
        {
            path: '/salesActivity/award',
            component: () => import('@/views/salesActivity/award'),
            name: '奖品管理',
            meta: {
                title: '奖品管理',
                icon: 'edit',
                roles: ['admin']
            },
        },
    ]

}

export default salesRouter
