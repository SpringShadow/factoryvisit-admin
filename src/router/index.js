import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Layout from '@/layout'

import marketRouter from './modules/market'
import picturesRouter from './modules/pictures'
import salesRouter from './modules/sales'
import customerRouter from './modules/customer'
import systemRouter from './modules/system'
import advertiseRouter from './modules/advertisement'
import printRouter from './modules/print'


export const constantRoutes = [{
        path: '/redirect',
        component: Layout,
        hidden: true,
        children: [{
            path: '/redirect/:path(.*)',
            component: () => import('@/views/redirect/index')
        }]
    },
    {
        path: '/login',
        component: () => import('@/views/login/index'),
        hidden: true
    },
    {
        path: '/auth-redirect',
        component: () => import('@/views/login/auth-redirect'),
        hidden: true
    },
    {
        path: '/404',
        component: () => import('@/views/error-page/404'),
        hidden: true
    },
    {
        path: '/401',
        component: () => import('@/views/error-page/401'),
        hidden: true
    },
    {
        path: '/',
        component: Layout,
        redirect: '/dashboard',
        children: [{
            path: 'dashboard',
            component: () => import('@/views/dashboard/index'),
            name: '首页',
            meta: {
                title: '首页',
                icon: 'home',
                affix: true
            }
        }]
    },
]

export const asyncRoutes = [
    marketRouter,
    picturesRouter,
    salesRouter,
    customerRouter,
    systemRouter,
    advertiseRouter,
    printRouter,
    {
        path: '*',
        redirect: '/404',
        hidden: true
    }
]

const createRouter = () => new Router({
    scrollBehavior: () => ({
        y: 0
    }),
    routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
    const newRouter = createRouter()
    router.matcher = newRouter.matcher // reset router
}

export default router
