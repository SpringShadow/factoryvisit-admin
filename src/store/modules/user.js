import {
    login,
    logout,
    getInfo
} from '@/api/user'
import {
    getToken,
    setToken,
    removeToken,
    setUserName
} from '@/utils/auth'
import router, {
    resetRouter
} from '@/router'

const state = {
    token: getToken(),
    name: '',
    avatar: '',
    introduction: '',
    roles: []
}

const mutations = {
    SET_TOKEN: (state, token) => {
        state.token = token
    },
    SET_INTRODUCTION: (state, introduction) => {
        state.introduction = introduction
    },
    SET_NAME: (state, name) => {
        state.name = name
    },
    SET_AVATAR: (state, avatar) => {
        state.avatar = avatar
    },
    SET_ROLES: (state, roles) => {
        state.roles = roles
    }
}

const actions = {
    login({
        commit
    }, userInfo) {
        const {
            login_name,
            password
        } = userInfo
        return new Promise((resolve, reject) => {
            // 测试
            login({
                login_name: login_name.trim(),
                password: password
            }).then(response => {
                const {
                    data
                } = response
                // 设置token  
                commit('SET_TOKEN', data.token)
                console.log(data.token);
                setToken(data.token,data.Expires)
                setUserName(data.user_name)
                // console.log(Data.user_name);
                // commit('SET_NAME',Data.user_name)
                // 设置权限
                resolve()
            }).catch(error => {
                reject(error)
            })
        })
    },

    // get user info
    getInfo({
        commit,
        state
    }) {
        return new Promise((resolve, reject) => {
            //测试

            const data={
                roles:['admin'],
                // name:'shadow',
                avatar:'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-uni-app-doc/7c946930-bcf2-11ea-b997-9918a5dda011.png',
                introduction:'管理后台项目'
            }
                commit('SET_ROLES', data.roles)
                commit('SET_AVATAR', data.avatar)
                commit('SET_INTRODUCTION', data.introduction)
                resolve(data)
            
            // getInfo(state.token).then(response => {
            //     const {
            //         data
            //     } = response

            //     if (!data) {
            //         reject('Verification failed, please Login again.')
            //     }

            //     const {
            //         roles,
            //         name,
            //         avatar,
            //         introduction
            //     } = data

            //     if (!roles || roles.length <= 0) {
            //         reject('getInfo: roles must be a non-null array!')
            //     }

            //     commit('SET_ROLES', roles)
            //     commit('SET_NAME', name)
            //     commit('SET_AVATAR', avatar)
            //     commit('SET_INTRODUCTION', introduction)
            //     resolve(data)
            // }).catch(error => {
            //     reject(error)
            // })
        })
    },

    // user logout
    logout({
        commit,
        state,
        dispatch
    }) {
        return new Promise((resolve, reject) => {
            // 测试
            commit('SET_TOKEN', '')
            commit('SET_ROLES', [])
            removeToken()
            resetRouter()
            
            dispatch('tagsView/delAllViews', null, {
                root: true
            })
            
            resolve()
            
            // logout(state.token).then(() => {
            //     commit('SET_TOKEN', '')
            //     commit('SET_ROLES', [])
            //     removeToken()
            //     resetRouter()

            //     dispatch('tagsView/delAllViews', null, {
            //         root: true
            //     })

            //     resolve()
            // }).catch(error => {
            //     reject(error)
            // })
        })
    },
    // remove token
    resetToken({
        commit
    }) {
        return new Promise(resolve => {
            commit('SET_TOKEN', '')
            commit('SET_ROLES', [])
            removeToken()
            resolve()
        })
    },
}

export default {
    namespaced: true,
    state,
    mutations,
    actions
}
