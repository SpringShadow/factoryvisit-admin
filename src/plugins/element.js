import Vue from 'vue'
import Element from 'element-ui'
import {Storage} from '../utils/index.js'

import '../styles/element-variables.scss'

Vue.use(Element, {
    size: Storage.getStorage('size') || 'medium', 
})