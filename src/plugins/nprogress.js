import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
NProgress.configure({
    showSpinner: false //是否显示进度条右侧加载的小圆环
})
export default NProgress
