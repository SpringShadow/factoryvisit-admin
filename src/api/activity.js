import request from '@/utils/request'
/**
     * 获取活动列表
     * @data 请求参数 
     * @return 返回值
 */
export function searchActivityList(data) {
    return request({
        url: '/api/Activity/list',
        method: 'post',
        data
    })
}
/**
     * 获取活动详情
     * @id 详情id
     * @return 返回值
 */
export function getActivityDeatil(id) {
    return request({
        url: '/api/Activity/detail',
        method: 'get',
        params: {
            id
        }
    })
}
/**
     * 新增和编辑活动
     * @data 请求参数 
     * @return 返回值
 */
export function createActivity(data) {
    return request({
        url: '/api/Activity/create',
        method: 'post',
        data
    })
}
/**
     * 获取活动流程
     * @id 活动流程id
     * @return 返回值
 */
export function createActivity(id) {
    return request({
        url: '/api/Activity/process',
        method: 'get',
        params: {
            id
        }
    })
}
/**
     * 编辑活动流程
     * @data 请求参数 
     * @return 返回值
 */
export function createActivity(data) {
    return request({
        url: '/api/Activity/eidt/process',
        method: 'post',
        data
    })
}

