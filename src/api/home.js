import request from '@/utils/request'

/**
     * 获取首页数据
     * @return 返回值
 */
export function getHomeData() {
    return request({
        url: '/api/home',
        method: 'get',

    })
}