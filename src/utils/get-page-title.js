import defaultSettings from '@/settings'

const title = defaultSettings.title || '云游相册'

export default function getPageTitle(pageTitle) {
    if (pageTitle) {
        return `${pageTitle} - ${title}`
    }
    return `${title}`
}
