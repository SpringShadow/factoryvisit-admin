import {
    Storage
} from './index.js'
import {
    TokenKey
} from '../config/index.js'

export function getToken() {
    return Storage.getStorage(TokenKey)
}

export function setToken(token, expire) {
    let expireTimeStamp = new Date(expire).getTime();
    let currentTimeStamp = new Date().getTime();
    let expireTime = parseInt((expireTimeStamp - currentTimeStamp) / (1000 * 60))
    return Storage.setStorage(TokenKey, token,expireTime)
}

export function removeToken() {
    return Storage.removeStorage(TokenKey)

}


export function getUserName() {
    return Storage.getStorage('userName')
}

export function setUserName(userName) {
    return Storage.setStorage('userName', userName)
}


