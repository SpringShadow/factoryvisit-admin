import Vue from 'vue'

import 'normalize.css/normalize.css' 

import './plugins/element.js'

import '@/styles/index.scss' 
import '@/styles/common.scss' 
import App from './App'
import store from './store'
import router from './router'

import './icons' 
import './permission' 
import './utils/error-log' 

import './plugins/filter.js'

Vue.config.productionTip = false

new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
})
